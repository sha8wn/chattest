chatTest
Created by Shawn Frank on 6/1/15.
Copyright (c) 2015 Shawn Frank. All rights reserved.

SUPPORTED iOS
chatTest supports iOS 7+ and has been tested on iOS 7 and iOS 8 devices.

Most of the development code is known to be compatible with both iOS versions and the only checks that were iOS specific were made for the AlertController and the Notification types

EXTERNAL LIBRARIES USED
chatTest makes use of a few different libraries configured and set up to complete the task at hand.

https://github.com/jessesquires/JSQMessagesViewController - was used for the chat UI

https://github.com/mrackwitz/MRProgress - was used for the progress hud

Parse iOS SDK was used to connect to the parse backend

CHAT MECHANISM
Currently the chat module makes use of push notifications as the mechanism to deliver real time information. 

HOW IT WAS MADE POSSIBLE

- when a user enters their name and before going to the chat screen, a user is created with a unique id and user name in the parse backend

- when a user creates a message, a parse “Cloud Code” script was created to run when ever a new object was saved into the Messages entity. Every time a new message is saved, all devices receive a push notification

- The device, based on the payload data of the push, understands how to handle the push notification and react. 

POSSIBLE IMPROVEMENTS

- currently there are no validations in place for checking name has been entered although this is not too difficult

- the weakness about relying on push is if a user decides to not give permission to the app for pushing, so another method would be more reliable