//
//  SFChatModel.h
//  chatTest
//
//  Created by Shawn Frank on 6/2/15.
//  Copyright (c) 2015 Shawn Frank. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "JSQMessages.h"

@interface SFChatModel : NSObject

@property (strong, nonatomic) NSMutableDictionary *chatUserData;

@property (strong, nonatomic) NSMutableArray *chatMessageData;

@property (strong, nonatomic) JSQMessagesBubbleImage *outgoingBubbleImageData;

@property (strong, nonatomic) JSQMessagesBubbleImage *incomingBubbleImageData;

- (void)addUser:(NSDictionary *)userInfo;

@end
