//
//  SFChatModel.m
//  chatTest
//
//  Created by Shawn Frank on 6/2/15.
//  Copyright (c) 2015 Shawn Frank. All rights reserved.
//

#import "SFChatModel.h"

@implementation SFChatModel

- (instancetype)init
{
    self = [super init];
    
    if(self)
    {
        self.chatUserData = [NSMutableDictionary dictionary];
        self.chatMessageData = [NSMutableArray array];
        
        JSQMessagesBubbleImageFactory *bubbleFactory = [[JSQMessagesBubbleImageFactory alloc] init];
        
        self.outgoingBubbleImageData = [bubbleFactory outgoingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleLightGrayColor]];
        
        self.incomingBubbleImageData = [bubbleFactory incomingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleRedColor]];
    }
    
    return self;
}

- (void)addUser:(NSDictionary *)userInfo
{
    //temp solution to handle people with same names
    
    NSMutableDictionary *currentUserChatProfile = [NSMutableDictionary dictionary];
    
    //set the user name
    [currentUserChatProfile setObject:[userInfo objectForKey:@"userName"] forKey:@"userName"];
    
    // Create the avatar
    NSString *initials = [self getInitials:[userInfo objectForKey:@"userName"]];
    
    [currentUserChatProfile setObject:[self createImageForInitials:initials] forKey:@"userImage"];
    
    [self.chatUserData setObject:currentUserChatProfile forKey:[userInfo objectForKey:@"userID"]];
}

- (NSString *)getInitials:(NSString*)fullName
{
    if(fullName.length > 0)
    {
        NSString *initals = [[[fullName componentsSeparatedByString:@" "]firstObject]substringToIndex:1];
        
        for(int i = 1; i < [[fullName componentsSeparatedByString:@" "]count]; i++)
        {
            NSString *token = [[fullName componentsSeparatedByString:@" "]objectAtIndex:i];
            
            initals = [initals stringByAppendingString:[token substringToIndex:1]];
        }
        
        return [initals uppercaseString];
    }
    
    return @"";
}

- (JSQMessagesAvatarImage*)createImageForInitials:(NSString*)initials
{
    JSQMessagesAvatarImage *avImage = [JSQMessagesAvatarImageFactory avatarImageWithUserInitials:initials
                                                                                 backgroundColor:[UIColor colorWithWhite:0.85f alpha:1.0f]
                                                                            textColor:[UIColor colorWithWhite:0.60f alpha:1.0f]
                                                                                 font:[UIFont systemFontOfSize:14.0f]
                                                                             diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
    
    return avImage;
}

@end
