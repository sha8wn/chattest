//
//  SFNameViewController.m
//  chatTest
//
//  Created by Shawn Frank on 6/1/15.
//  Copyright (c) 2015 Shawn Frank. All rights reserved.
//

#import "SFNameViewController.h"
#import "SFChatViewController.h"
#import <MRProgress/MRProgressOverlayView.h>
#import <Parse/Parse.h>

@interface SFNameViewController ()

@end

@implementation SFNameViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setTranslucent:NO];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.title = @"Join Chat";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)enterChatAction:(id)sender
{
    if([UIAlertController class])
    {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"ALERT"
                                              message:@"Please enter your user name"
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
         {
             textField.placeholder = @"Enter Username";
         }];
        
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:@"Cancel"
                                       style:UIAlertActionStyleCancel
                                       handler:^(UIAlertAction *action)
                                       {
                                           NSLog(@"Cancel action");
                                       }];
        
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:@"Submit"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       UITextField *userNameTextfield = (UITextField*)[alertController.textFields firstObject];
                                       userSpecifiedName = userNameTextfield.text;
                                       
                                       [self getToChatRoom];
                                   }];
        
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"ALERT" message:@"Please enter your user name" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
        
        [alert setAlertViewStyle:UIAlertViewStylePlainTextInput];
    
        [alert show];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1)
    {
        UITextField *userNameTextfield = (UITextField*)[alertView textFieldAtIndex:0];
        
        userSpecifiedName = userNameTextfield.text;
        
        [self getToChatRoom];
    }
}

- (void)getToChatRoom
{
    //Add Hud to window
    UIWindow *window = [[[UIApplication sharedApplication]windows]objectAtIndex:0];
    
    [MRProgressOverlayView showOverlayAddedTo:window animated:YES];
    
    PFObject *ChatRoomUser = [PFObject objectWithClassName:@"ChatRoomUsers"];
    ChatRoomUser[@"userName"] = userSpecifiedName;
    
    [ChatRoomUser saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error)
    {
        [MRProgressOverlayView dismissAllOverlaysForView:window animated:YES];
        
        if (succeeded)
        {
            // The object has been saved.
            SFChatViewController *chatVC = [self.storyboard instantiateViewControllerWithIdentifier:@"chatVC"];
            
            chatVC.userName = userSpecifiedName;
            chatVC.ChatRoomUser = ChatRoomUser;
            
            
            
            [self.navigationController pushViewController:chatVC animated:YES];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"ERROR" message:@"Unable to join chatroom at this time, please try again later" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
            
            [alert show];
        }
    }];
    
    
}

@end
