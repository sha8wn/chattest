//
//  SFNameViewController.h
//  chatTest
//
//  Created by Shawn Frank on 6/1/15.
//  Copyright (c) 2015 Shawn Frank. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SFNameViewController : UIViewController <UIAlertViewDelegate>
{
    NSString *userSpecifiedName;
}

- (IBAction)enterChatAction:(id)sender;

@end
