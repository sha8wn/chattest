//
//  SFChatViewController.h
//  chatTest
//
//  Created by Shawn Frank on 6/1/15.
//  Copyright (c) 2015 Shawn Frank. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <JSQMessagesViewController/JSQMessages.h>
#import "SFChatModel.h"
#import <Parse/Parse.h>
#import <Bolts/Bolts.h>

@interface SFChatViewController : JSQMessagesViewController <JSQMessagesCollectionViewDataSource, JSQMessagesCollectionViewDelegateFlowLayout, UITextViewDelegate>

@property (strong, nonatomic) NSString *userName;
@property (strong, nonatomic) NSString *userId;
@property (strong, nonatomic) PFObject *ChatRoomUser;

@property (strong, nonatomic) SFChatModel *chatData;

@end
